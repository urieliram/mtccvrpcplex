// --------------------------------------------------------------------------------
// File: MTCCVRP.cpp
// Based on paper: A customer-centric routing problem with multiple trips of a single vehicle
// Authors:  Iris Martinez-Salazar, Francisco Angel-Bello and Ada Alvarez
//
// Developers:Citlalli M. Olvera e Uriel I. Lezama
// --------------------------------------------------------------------------------

#include <ilcplex/ilocplex.h>
#include <time.h>

ILOSTLBEGIN

typedef IloArray<IloNumVarArray> NumVarMatrix;
typedef IloArray<NumVarMatrix>   NumVar3Matrix;

typedef IloArray<IloNumArray>    FloatMatrix;
typedef IloArray<IloNumVarArray> NumVarMatrix;
int *SOrder(double *weights, int num);
double CalcDur(int no, int *sp, FloatMatrix s, IloNumArray p);
double NNheuristic(int n, int *sp, FloatMatrix s, IloNumArray p);

int main(int argc, char **argv) {
	IloEnv env;

	try {

		clock_t itime, ftime;
		double gtime, dur, M;
		int *sec;
		IloInt i, j; // Indices de ciudades		
		IloInt k;    // Indices de niveles en la red multinivel
		IloInt n;    // Numero de ciudades sin contar la ciudad de origen
		IloInt m;    // Numero minimo de viajes para obtener una solucion factible
		IloNum D;    // Duración del turno de trabajo
        IloNum Q;    // Capacidad del vehiculo
		IloInt N;    // nodos reales + nodos depot - regreso

		double maxp, maxs, T, *w1;
		int *l1;
		char comment[100];
		const char *filename, *solution, *tabla;
		if (argc == 4) {
			filename = argv[1];
			solution = argv[2];
			tabla    = argv[3];
		} else {
			cout << "Debe incluir los archivos de datos, de solucion y de tabla. \n Sugerencia ./mtccvrp instancia.txt solucion.txt tabla.txt true" << endl;
			exit(1);
		}
		
		// LECTURA DE DATOS DESDE ARCHIVO ***********************************************************
		ifstream file;
		file.open(filename);
		if (!file) {
			cerr << "ERROR: could not open file '" << filename
				<< "' for reading" << endl;
			cerr << "usage:   " << argv[0] << " <file>" << endl;
			throw(-1);
		}
		file >> comment >> n;	// nbClients
		file >> comment >> m;	// nbTrips
        file >> comment >> Q;	// VehCapacity
		file >> comment;          
		IloNumArray d0(env, n + 1); // ClientDemands 
		for (i = 1; i < n + 1; i++) file >> d0[i];
		d0[0] = 0;
		file >> comment;        
		IloNumArray s0(env, n + 1); // ServiceTimes
		for (i = 1; i < n + 1; i++) file >> s0[i];
		s0[0] = 0;
		file >> comment;        
		FloatMatrix t(env, n + 1); // TravelTimes
		for (i = 0; i < n + 1; i++)
			t[i] = IloNumArray(env, n + 1);
		for (i = 0; i < n + 1; i++) {
			for (j = 0; j < n + 1; j++)
				file >> t[i][j];
		}	
		file.close();


		// SE IMPRIMEN ENTRADAS *********************************************************************

		// cout << "ClientDemands:" << endl;		
		// for(i = 0 ; i < n + 1; i++){
		// 	cout << "\t" << d0[i];
 		// }
		// cout << "\n ServiceTimes:" << endl;		
		// for(i = 0 ; i < n + 1; i++){
		//  	cout << "\t" << s0[i];
 		// }
		// cout << "\n TravelTimes:" << endl;		
		// for (i = 0; i < n + 1; i++) {
		// 	for (j = 0; j < n + 1; j++)			
		// 	cout << "\t" << t[i][j];			
		// 	cout << "\n" << endl;
		// }	
		// cout << " " << endl;

		// SE DETERMINAN PARAMETROS *********************************************************************

		m = 3;	        // nbTrips
        D = 480 ;	    // WorkingShift 8h=480min , 

		// Se calcula el total de nodos
		N = n + m - 1;
		
		cout << "nbClients, nbTrips, VehCapacity, WorkingShift, N:   " << n << " , " << m << " , " << Q << " , " << D << " , " << N << endl;


		// Se calcula la matriz de costos como tiempo de servicio mas traslado
		FloatMatrix c0(env, n + 1); 
		for (i = 0; i < n + 1; i++)
			c0[i] = IloNumArray(env, n + 1 );
		for (i = 0; i < n + 1; i++) {
			for (j = 0; j < n + 1; j++)
			if(i!=j) c0[i][j] = t[i][j]; // + s0[i] ;
		}
		
		for (i = 0; i < n + 1; i++) {
			for (j = 0; j < n + 1; j++) {
				if(i!=j) c0[i][j] = c0[i][j] + s0[i];
			}
		}


		// // Se imprime la matriz de costos original
		// cout << "\n Costs:" << endl;		
		// for (i = 0; i < n + 1; i++) {
		// 	for (j = 0; j < n + 1; j++)			
		// 	cout << "\t" << c0[i][j];			
		// 	cout << "\n" << endl;
		// }	
		// cout << " " << endl;


	// Se calcula la matriz de costos extendida con replicas de el nodo 0 o depósito ******************

	// Se extiende la matriz a NxN
	FloatMatrix c(env, N + 1); 
	for (i = 0; i < N + 1; i++)
		c[i] = IloNumArray(env, N + 1 );
	for (i = 0; i < N + 1; i++) {
		for (j = 0; j < N + 1; j++)
			if (i != j)
				c[i][j] = 0;
	}
	//primer cuadrante
	for (i = 0; i < n + 1; i++) {
		for (j = 0; j < n + 1; j++)	
		if (i != j)		
		c[i][j] = c0[i][j];
	}	
	//segundo cuadrante
	for (i = n + 1; i < N + 1; i++) {
		for (j = 1; j < n + 1; j++)			
		c[i][j] = c0[0][j];
	}
	//tercer cuadrante
	for (i = 1; i < n + 1; i++){ 
		for (j = n + 1; j < N + 1; j++)			
		c[i][j] = c[i][0];
	}
	//cuarto cuadrante
	for (i = n + 1; i < N + 1; i++) {
		for (j = n + 1; j < N + 1; j++)			
		c[i][j] = 0;
	}

	// se imprime la matriz de costos extendida
	cout << "\n Matriz de costos extendida:" << endl;		
	for (i = 0; i < N + 1; i++) {
		cout << i ;
		for (j = 0; j < N + 1; j++)			
			cout << "\t" << c[i][j];			
			cout << "\n" << endl;
		}	
	cout << " " << endl;	

	// Se calcula la matriz de demanda y tiempo de servicio extendida ******************

	IloNumArray d(env, N + 1 ); // Client Demands Extend
	for (i = 0; i < N + 1; i++) d[i] = 0;
	for (i = 1; i < n + 1; i++) d[i] = d0[i];

	 cout << "ClientDemandsExtend:" << endl;		
	 for(i = 0 ; i < N + 1; i++){
	 	cout << i << "\t" << d[i] << endl;
 	 }
	 cout << "" << endl;	
	 
	 IloNumArray s(env, N + 1); // Service Times Extend
	 for (i = 0; i < N + 1; i++) s[i] = 0;
	 for (i = 1; i < n + 1; i++) s[i] = s0[i];


	// VARIABLES PARA EL MODELO *********************************************************************

	// Secuencia solucion
	// sec = new int[n + 1]; 
	// for (i = 0; i < n + 1; i++) 
	// sec[i] = 0;

	// Variable x[i,k]
	NumVarMatrix x(env, N + 1);
	for (k = 0; k < N + 1; k++)
		x[k] = IloNumVarArray(env, N + 1, 0, 1, ILOINT);
		// x[k] = IloNumVarArray(env, N + 1, 0, 1);          // LR
	for(i = 0; i < N + 1; i++) {			
		for(j = 0; j < N + 1; j++) {
		char indice1[5], indice2[5]; //Naming
		sprintf(indice1, "%d", i);  sprintf(indice2, "%d", j);  char nombre[20] = "x["; //Naming
		strcat(nombre, indice1);  strcat(nombre, ",");	 strcat(nombre, indice2); strcat(nombre, "]"); //Naming
		x[i][j].setName(nombre); //Naming
		}
	}

			
	/* define the num vars here for the 3-D matrix */
	/* https://www.ibm.com/support/pages/sample-create-and-use-multi-dimensional-ilonumvararray */
	NumVar3Matrix y(env, N + 1);
	/* initialize this matrix */
	for(i = 0; i< N + 1; i++) {
		y[i] = NumVarMatrix(env, N + 1);
		for(j = 0; j < N + 1; j++) {
			y[i][j] = IloNumVarArray(env, N );
			for(k = 0; k < N ; k++) {
				y[i][j][k] = IloNumVar(env, 0.0, 1);
				char indice1[5], indice2[5], indice3[5], indice4[5]; //Naming
				sprintf(indice1, "%d", i);  sprintf(indice2, "%d", j);  sprintf(indice3, "%d", k);	 char nombre[20] = "y["; //Naming
				strcat(nombre, indice1);  strcat(nombre, ",");	 strcat(nombre, indice2); strcat(nombre, ","); strcat(nombre, indice3);strcat(nombre, "]"); //Naming
				y[i][j][k].setName(nombre); //Naming
			}
		}
	}

	// Variable w[i] de demanda acumulada
	double aux = D * N ;
	IloNumVarArray w(env, N + 1, 0.0, aux);

	for(i = 0; i < N + 1; i++) {				
		char indice1[5]; //Naming
		sprintf(indice1, "%d", i);  char nombre[20] = "w["; //Naming
		strcat(nombre, indice1); strcat(nombre, "]"); //Naming
		w[i].setName(nombre); //Naming		
	}


	cout << "Termina definición de variables cplex " << endl;
		

	// RESTRICCIONES DEL MODELO *********************************************************************

	// Se declara el modelo
	IloModel model(env);

	//                                                        Rest( 14 )
	// Guarantee that each node occupies a single position in any feasible solution
	for (i = 1; i < N + 1; i++) {
		IloExpr v(env);
		for (k = 1; k < N + 1; k++)	v += x[i][k];
		model.add(v == 1).setName("R14");
		v.end();
	}

	//                                                        Rest( 15 )
	// Guarantee that no more than one node occupies each position.
	for (k = 1; k < N + 1; k++) {
		IloExpr v(env);
		for (i = 1; i < N + 1; i++) v += x[i][k];
		model.add(v == 1).setName("R15");
		v.end();
	}
		
	//                                                        Rest( 16 )
	// Ensure that at level k can leave arcs from active nodes.
	for (i = 1; i < N + 1; i++) {			
		for (k = 1; k < N ; k++) {
			IloExpr v(env);
			for (j = 1; j < N + 1 ; j++)				
				if(j!=i) v += y[i][j][k];

			char indice1[5], indice2[5], indice3[5]; //Naming
			sprintf(indice1, "%d", i); sprintf(indice2, "%d", j);sprintf(indice3, "%d", k);//Naming
			char nombre[15] = "R16_"; strcat(nombre, indice1); strcat(nombre, "_"); strcat(nombre, indice3); //Naming
			model.add( v == x[i][k] ).setName(nombre);
			v.end();
		}

	}

	//                                                        Rest( 17 )
	// Impose that at position k+ 1 can arrive arcs to active nodes. 
	for (i = 1; i < N + 1; i++) {			
		for (k = 1; k < N ; k++) {
			IloExpr v(env);
			for (j = 1; j < N + 1; j++)
				if(i!=j) v += y[j][i][k];

			char indice1[5], indice2[5], indice3[5]; //Naming
			sprintf(indice1, "%d", i); sprintf(indice2, "%d", j);sprintf(indice3, "%d", k);//Naming
			char nombre[15] = "R17_"; strcat(nombre, indice1); strcat(nombre, "_"); strcat(nombre, indice2); strcat(nombre, "_"); strcat(nombre, indice3); //Naming
			model.add( v == x[i][k+1] ).setName(nombre);
			v.end();
		}
	}

	//                                                        Rest( 18 ) 
	// Ensures that the time limit D of a working shift is not exceeded. 			
	IloExpr v(env);
	for (i = 1; i < N + 1; i++)	v+= c[0][i] * x[i][1];
	for (i = 1; i < N + 1; i++)	{	
		for (j = 1; j < N + 1; j++)	{
			if (j != i) 						
				for (k = 1; k < N ; k++) v+= c[i][j] * y[i][j][k]; // k < N 
		}
	}
	for (i = 1; i < N + 1; i++)	v+= c[i][0] * x[i][N];		
	model.add( v <= D ).setName("R18");
	v.end();

	// 		                                                 Rest( 19 )
	// Compute the cumulative demand and guarantee that the sum of the
	// demands of nodes between two copies of the depot is less than or
	// equal to Q.                                             
	IloExpr v2(env);
	for (i = 1; i < n + 1; i++) {
		v2 += d[i] * x[i][1];
	}
	model.add( v2 == w[1] ).setName("R19");
	v2.end();

	//											 			Rest( 20 )
	// Compute the cumulative demand and guarantee that the sum of the
	// demands of nodes between two copies of the depot is less than or
	// equal to Q.                                             
	for (k = 1; k < N; k++) {
		IloExpr v(env);
		v+= w[ k ];
		for (i = 1; i < n + 1; i++) v+= x[i][k+1] * d[i];
		for (i = 1; i < n + 1; i++) v+= x[i][k+1] * Q ;
		v+= - Q;
		//model.add(v <= w[ k + 1 ]).setName("R20");
		model.add(v <= w[ k + 1 ]).setName("R20");
		v.end();
	}

	//											 			Rest( 21 )
	// Complemento de restriccion-20.
	for (k = 1; k < N ; k++) {
		IloExpr v(env);
		for (i = 1; i < n + 1; i++) v+= Q * ( x[i][k + 1] );
		model.add( v >= w[ k + 1 ] ).setName("R21");
		v.end();
	}

		
	cout << "Termina restricciones cplex" << endl;


	// FUNCION OBJETIVO *********************************************************************

	IloExpr obj(env);
	for (i = 1; i < N + 1 ; i++) {
		obj+= c[0][i] * x[i][1] * N; 
	} 
	for (i = 1; i < N + 1 ; i++){	
		for (j = 1; j < N + 1; j++)	{
			if (j != i) 						
				for (k = 1; k < N ; k++) obj+= c[i][j] * (N - k) * y[i][j][k];
		}
	}

	cout << "Termina funcion objetivo cplex " << endl;

	model.add(IloMinimize(env, obj));
	obj.end();


	// SE RESUELVE EL MODELO ****************************************************************

	IloCplex cplex(env);
	cplex.extract(model);
	cplex.exportModel("modelo.lp");
	//cplex.setOut(env.getNullStream());
	IloNum tolerance = cplex.getParam(IloCplex::EpInt);
	itime = clock();
	cplex.solve();

	cout << "Termina resuelve modelo cplex " << endl;


	// SE IMPRIME LA SOLUCION ***************************************************************

	ftime = clock();
	gtime = (double)(ftime - itime) / CLOCKS_PER_SEC;
	IloNum ObjVal;
	ObjVal = cplex.getObjValue();
	cplex.out() << "Optimal value: " << ObjVal << endl;
	ofstream sol, table;
	sol.open(solution, ios::app);
	if (!sol) {
		cerr << "Error: Unable to open 'soluciones.txt' file." << endl;
		exit(1);
	}
	table.open(tabla, ios::app);
	if (!table) {
		cerr << "Error: Unable to open 'tabla.txt' file." << endl;
		exit(1);
	}

	sol << "Instance name: " << argv[1] << endl;
	sol << "Optimal value: " << ObjVal << endl;
	sol << "CPU time:      " << gtime << endl;	

	if (cplex.getStatus() == IloAlgorithm::Feasible || cplex.getStatus() == IloAlgorithm::Optimal ) {	
			w1 = new double[N+1];
			
		cout << "Demanda acumulada:"<< endl;
		for (i = 1; i < N + 1; i++) {
			w1[i] = cplex.getValue(w[i]);
			cout << w1[i]  << " - " ;		
		}	
		cout << "" <<  endl;	
		cout << "Secuencia de los clientes:"<< endl;
		for (i = 1; i < N + 1; i++) {
			for (j = 1; j < N + 1; j++) {
				if( cplex.getValue(x[j][i]) > 0 ) {
					if (j<=n){
					 cout << j << " - ";
					}else
					
					{
					 cout << 0 << " - ";						
					}
					
				}
			}
		}
		cout << "" <<  endl;	


	}else{	
		cout <<  "Error in cplex:"<< cplex.getStatus() << endl << endl;  
		cplex.end(); 
		return 0;
	}
	
		
	// l1 = new int[n];
	// l1 = SOrder(u1, n);
	// sec = new int[n + 1];
	// sec[0] = 0;
	// for (i = 0; i < n; i++) sec[i + 1] = l1[i] + 1;
	// dur = CalcDur(n, sec, t, s);
	// sol << "Order ";
	// for (i = 0; i < n; i++) sol << l1[i] + 1 << "  ";
	// sol << endl << "******************************" << endl;
	// table << argv[1] << "	" << ObjVal << "	" << dur << "	" << gtime << endl;
	// sol.close();
	// table.close();


	}
	catch (IloException& e) {
		cerr << " ERROR1: " << e << endl;
	}
	catch (...) {
		cerr << " ERROR2" << endl;
	}
	env.end();
	return 0;
}

int *SOrder(double *weights, int num)
/* Orders an array of num elements (double).
   Returns an array with the indexes ordered for low to high */
{
	int cnt, j, i, tempi, *index;
	double temp, *cost;
	cost = new double[num];
	index = new int[num];
	for (i = 0; i < num; i++) {
		cost[i] = weights[i];
		index[i] = i;
	}
	cnt = num;
	while (cnt > 0) {
		cnt = 0;
		for (j = 0; j < num - 1; j++) {
			if (cost[j] > cost[j + 1]) {
				temp = cost[j + 1];
				cost[j + 1] = cost[j];
				cost[j] = temp;
				tempi = index[j + 1];
				index[j + 1] = index[j];
				index[j] = tempi;
				cnt++;
			}
		}
	}
	delete[] cost;
	return index;
}

double CalcDur(int no, int *sp, FloatMatrix s, IloNumArray p)
{
	int i;
	double dur;
	dur = s[sp[0]][sp[1]] + p[sp[1]];
	for (i = 1; i < no; i++) dur += s[sp[i]][sp[i + 1]] + p[sp[i + 1]];
	return dur;
}

double NNheuristic(int n, int *sp, FloatMatrix s, IloNumArray p)
{
	int i, j, k, h, cnt, *sa, m1, m2;
	double dur;
	sa = new int[n + 1];
	for (i = 0; i < n + 1; i++) sa[i] = i;
	cnt = n; k = 0; dur = 0;
	while (cnt) {
		m1 = s[sp[k]][sa[1]]+p[sa[1]];
		j = sa[1]; h = 1;
		for (i = 2; i < cnt + 1; i++) {
			m2 = s[sp[k]][sa[i]] + p[sa[i]];
			if (m1 > m2) {
				m1 = m2; j = sa[i]; h = i;

			}
		}
		sp[k + 1] = j; dur += m1;
		for (i = h; i < cnt; i++) sa[i] = sa[i + 1];
		k++; cnt--;
	}
	return dur;
}

